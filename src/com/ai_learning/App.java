package com.ai_learning;

/**
 * Created by dancinggrass on 11/13/15.
 */

import com.ai_learning.data.*;
import com.ai_learning.data.parser.*;
import com.ai_learning.NB;

public class App {
    public void run() {
        Parser parser = new Parser("car.data", ",");
        DataFrame dataframe = parser.toDF();
        for (Instance instance : dataframe) {
            for (String field : instance) {
                System.out.print(field);
                System.out.print(',');
            }
            System.out.println();
        }
    }
    
    public void run(String algorithm, String trainFile, String testFile) {
        Parser trainParser = new Parser(trainFile, ",");
        DataFrame trainDataFrame = trainParser.toDF();
        
        Parser testParser = new Parser(testFile, ",");
        DataFrame testDataFrame = testParser.toDF();
        
        switch (algorithm) {
            case "nb": {
                NB nb = new NB(6);
                nb.make(trainDataFrame);
                nb.run(testDataFrame);
                //System.out.println("CORRECT = " + (double)nb.correct() / (double)testDataFrame.size());
                System.out.println("RELATION NAME: " + testDataFrame.getRelationName());
                for (AttributeKnowledge ak : testDataFrame.getAttributes()) {
                    System.out.print(ak.getName() + ": ");
                    System.out.println(ak.getAllowedValues());
                }
                System.out.println();
                
                System.out.println("CORRECT: " + nb.correct());
                nb.printConfusionMatrix();
            }
        }
    }
}
